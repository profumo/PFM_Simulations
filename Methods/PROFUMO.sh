#!/bin/sh

# Runs PROFUMO on simulated dataset
# Usage: PROFUMO.sh <output_dir> <nifti_dir> <dim> <TR>

# SPDX-License-Identifier: Apache-2.0

output_dir=$1
nifti_dir=$2
dim=$3
TR=$4

mkdir "${output_dir}"

# Will need adjusting!
profumo_dir=~/FMRIB/Code/PROFUMO

# Run PROFUMO analysis
"${profumo_dir}/C++/PROFUMO" \
    "${nifti_dir}/PROFUMO_SpecFile.json" \
    "${dim}" \
    "${output_dir}/Analysis.pfm" \
    --useHRF "${TR}" --hrfFile "${profumo_dir}/HRFs/Default.phrf" \
    --covModel Run --lowRankData 150 -d 0.5 --globalInit --nThreads 10 \
    > "${output_dir}/TerminalOutput-pfm.txt"

# Run postprocessing
"${profumo_dir}/Python/postprocess_results.py" \
    "${output_dir}/Analysis.pfm" \
    "${output_dir}/Results.ppp" \
    "${nifti_dir}/Reference.nii.gz" \
    > "${output_dir}/TerminalOutput-ppp.txt"
