function [ Pg, P, A, pcA ] = loadPROFUMO( pfmDir, params )

% SPDX-License-Identifier: Apache-2.0

pfmDir = fullfile(pfmDir, 'Results.ppp');

% Load group maps
Pg = read_avw(fullfile(pfmDir, 'Maps', 'Group.nii.gz'));
Pg = reshape(Pg, params.V, params.iN);

% And subject maps / timecourses
P = cell(params.S, 1);
A = cell(params.S, 1);
pcA = cell(params.S, 1);
for s = 1:params.S
    subj = sprintf('sub-S%02d',s);
    
    % Subject maps
    P{s} = read_avw(fullfile(pfmDir, 'Maps', [subj '.nii.gz']));
    P{s} = reshape(P{s}, params.V, params.iN);
    
    % Time courses
    A{s} = cell(params.R(s), 1);
    pcA{s} = cell(params.R(s), 1);
    for r = 1:params.R(s)
        run = sprintf('run-R%02d',r);
        
        % Timecourses
        A{s}{r} = csvread(fullfile(pfmDir, 'TimeCourses', [subj '_' run '.csv']))';
        % Modulate by amplitudes
        h = csvread(fullfile(pfmDir, 'Amplitudes', [subj '_' run '.csv']));
        A{s}{r} = A{s}{r} .* h;
        
        % Temporal netmats
        pcA{s}{r} = csvread(fullfile(pfmDir, 'NetMats', [subj '_' run '.csv']));
    end
end
